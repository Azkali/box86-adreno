FROM arm32v7/ubuntu:latest
 
# Set environment
ARG DEBIAN_FRONTEND=noninteractive

# Set needed ENV
ENV LANGUAGE="C" LC_ALL="C"
ENV GALLIUM_DRIVER=zink MESA_LOAD_DRIVER_OVERRIDE=zink
ENV STEAMOS=1 STEAM_RUNTIME=1
ENV WINEPREFIX="/root/.wine" WINEARCH="win32"

# Enable deb-src
RUN sed -i 's/# deb-src/deb-src/g' /etc/apt/sources.list

# Install build dependencies
RUN apt-get update -y && apt-get install -y \
	git \
	cmake \
	build-essential \
	python3 \
	pciutils \
	mesa-utils \
	libxext6 \
    	libx11-6 \
    	libglvnd0 \
    	libgl1 \
    	libglx0 \
    	libegl1 \
    	libxext6 \
	libx11-6 \
	libtcmalloc-minimal4 \
	wget \
	libnm0 \
	zenity \
	nginx \
	libgtk2.0-0 \
	libdbus-glib-1-2 \
	libxss1 \
	libdbusmenu-gtk4 \
	libsdl2-2.0-0 \
	libice6 \
	libsm6 \
	libopenal1 \
	libusb-1.0-0 \
	libappindicator1

# Install mesa build dependencies
RUN apt build-dep -y mesa

# Build box86
RUN git clone https://github.com/ptitSeb/box86 && \
	mkdir -p box86/build && \
	cd box86/build && \
	cmake .. -DARM_DYNAREC=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo && \
	make -j$(nproc) install

# Build mesa
RUN git clone https://gitlab.freedesktop.org/mesa/mesa && \
	cd mesa && \
	meson build -Dplatforms=x11,wayland -Dgallium-drivers=swrast,zink,freedreno -Dvulkan-drivers=freedreno -Ddri3=enabled -Degl=enabled -Dgles2=enabled -Dglvnd=true -Dglx=dri -Dosmesa=true -Dshared-glapi=enabled -Dvalgrind=disabled -Dgles1=disabled -Dfreedreno-kgsl=true -Dcpp_rtti=false && \
	ninja -j$(nproc) -C build install

# Steamcmd setup
RUN wget https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz
RUN tar zxvf steamcmd_linux.tar.gz -C /
RUN rm steamcmd_linux.tar.gz

# Steam setup
RUN wget http://media.steampowered.com/client/installer/steam.deb
RUN ar x steam.deb
RUN tar xf data.tar.xz -C /
RUN rm steam.deb

# Wine setup
RUN wget https://www.playonlinux.com/wine/binaries/phoenicis/upstream-linux-x86/PlayOnLinux-wine-7.0-rc2-upstream-linux-x86.tar.gz
RUN tar -xzvf PlayOnLinux-wine-7.0-rc2-upstream-linux-x86.tar.gz -C /usr/
RUN rm PlayOnLinux-wine-7.0-rc2-upstream-linux-x86.tar.gz

# Winetricks setup
RUN wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks -O /usr/bin/winetricks
RUN chmod +x /usr/bin/winetricks

CMD bash
